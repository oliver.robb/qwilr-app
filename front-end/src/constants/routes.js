export const HOME = '/';
export const REGISTER = '/register';
export const LOGIN = '/login';
export const PORTFOLIO = '/portfolio';
export const TRADE = '/trade';
