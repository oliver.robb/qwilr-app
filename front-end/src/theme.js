import { createMuiTheme } from "@material-ui/core/styles";
import grey from "@material-ui/core/colors/grey";

const rawTheme = createMuiTheme({
  palette: {
    primary: {
      light: "#68ccc6	",
      main: "#03AAA0",
      dark: "#027770"
    },
    secondary: {
      light: "#f7a7ac",
      main: "#F26C75",
      dark: "#a94c52"
    },
    tertiary: {
      light: "#6d8281	",
      main: "#0B2F2D",
      dark: "#08211f"
    },
    white: {
      main: "#ffffff"
    },
    background: {
      main: "#282c34"
    },
    contrastThreshold: 3,
    tonalOffset: 0.1
  },
  typography: {
    fontFamily: "'Work Sans', sans-serif",
    fontSize: 16,
    fontWeightLight: 300, // Work Sans
    fontWeightRegular: 400, // Work Sans
    fontWeightMedium: 550, // Roboto Condensed
    fontWeightHeavy: 800, // Roboto Condensed
    fontFamilySecondary: "'Roboto Condensed', sans-serif",
    useNextVariants: true
  }
  // spacing: {
  //     unit: 8
  // }
});

const fontHeader = {
  color: rawTheme.palette.text.primary,
  fontWeight: rawTheme.typography.fontWeightMedium,
  fontFamily: rawTheme.typography.fontFamilySecondary,
  textTransform: "uppercase"
};

const theme = {
  ...rawTheme,
  palette: {
    ...rawTheme.palette,
    background: {
      ...rawTheme.palette.background,
      default: rawTheme.palette.common.white,
      placeholder: grey[200]
    }
  },
  typography: {
    ...rawTheme.typography,
    fontHeader,
    h1: {
      ...rawTheme.typography.h1,
      ...fontHeader,
      letterSpacing: 10,
      fontSize: 60
    },
    h2: {
      ...rawTheme.typography.h2,
      ...fontHeader,
      letterSpacing: 8,
      fontSize: 48
    },
    h3: {
      ...rawTheme.typography.h3,
      ...fontHeader,
      fontSize: 42
    },
    h4: {
      ...rawTheme.typography.h4,
      ...fontHeader,
      fontSize: 36
    },
    h5: {
      ...rawTheme.typography.h5,
      fontSize: 20,
      fontWeight: rawTheme.typography.fontWeightRegular
    },
    h6: {
      ...rawTheme.typography.h6,
      ...fontHeader,
      fontSize: 18
    },
    subtitle1: {
      ...rawTheme.typography.subtitle1,
      fontSize: 30
    },
    footer: {
      ...rawTheme.typography.subtitle1,
      fontSize: 20
    },
    body1: {
      ...rawTheme.typography.body2,
      fontWeight: rawTheme.typography.fontWeightRegular,
      fontSize: 16
    },
    body2: {
      ...rawTheme.typography.body1,
      fontSize: 16,
      fontWeight: rawTheme.typography.fontWeightMedium
    }
  }
};

export default theme;
