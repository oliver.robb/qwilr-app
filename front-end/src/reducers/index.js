const defaultState = {
  user: null,
  cashBalance: null,
  portfolio: null
};

const mainReducer = (state = defaultState, action) => {
  switch (action.type) {
        case "LOG_OUT": {
      return defaultState;
    }
    case "SET_CASHBALANCE": {
      return {
        ...state,
        cashBalance: action.res
      };
    }
    case "SET_USER": {
      return {
        ...state,
        user: action.res
      };
    }
    case "SET_PORTFOLIO": {
      return {
        ...state,
        portfolio: action.res
      };
    }
    default: {
      return state;
    }
  }
};

export default mainReducer;
