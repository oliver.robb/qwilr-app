import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "../components/typography";
import {
  Grid,
  Table,
  Paper,
  TableCell,
  TableHead,
  TableRow,
  TableBody
} from "@material-ui/core";
import { connect } from "react-redux";
import * as actionCreators from "../actions";
import { compose } from "recompose";
import Axios from "axios";

const styles = theme => ({
  h1: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(2)
    }
  },
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
});

class PortfolioView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rows: []
    };
  }

  componentWillReceiveProps = async ({ portfolio }) => {
    let data = [];
    if (portfolio) {
      for (var key in portfolio) {
        if (portfolio.hasOwnProperty(key)) {
          let item = portfolio[key];
          data.push(
            await this.createData(key, item.cost, item.count, item.prev)
          );
        }
      }
    }
    if (data.length > 0) {
      this.setState({ rows: data });
    }
  };

  getAxios = async (code, prev_high) => {
    let apikey = "2MZ0F3RWHCS07MFK";
    let url = `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${code}&apikey=${apikey}`;
    let price = -1;
    await Axios.get(url)
      .then(stock => {
        price = stock.data["Global Quote"]["05. price"];
      })
      .catch(e => {
        console.log(e);
        price = prev_high;
      });

    return price;
  };

  componentWillUnmount = () => {
    let data = {}
    this.state.rows.map(stock => {
      console.log(stock);
        let stockInfo = {
            cost: stock.cost,
            count: stock.count,
            prev: stock.price,
            value: stock.price * stock.count,
        }
        console.log(stockInfo);
        data[stock.code] = stockInfo

    })
    this.props.updateUser(this.props.user, this.props.cashBalance, data)
  }

  createData = async (code, cost, count, prev) => {
    let price = await this.getAxios(code, prev);
    let value = (count * price);
    let valueS = (value * 1).toFixed(2);
    let priceS = (1 * price).toFixed(2);
    let costS = (cost * 1).toFixed(2);
    return { code, cost, count, price, value, costS, priceS, valueS };
  };

  render() {
    const { classes } = this.props;

    return (
      <Grid container alignItems="center" justify="center">
        <Grid item>
          <Typography
            color="secondary"
            align="center"
            variant="h2"
            marked="center"
            className={classes.h1}
          >
            Holdings
          </Typography>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Stock</TableCell>
                  <TableCell align="right">Purchase Price ($/share)</TableCell>
                  <TableCell align="right">Count</TableCell>
                  <TableCell align="right">Price ($/share)</TableCell>
                  <TableCell align="right">Value ($)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.rows.map(row => (
                  <TableRow key={row.code}>
                    <TableCell align="left">{row.code}</TableCell>
                    <TableCell align="right">${row.costS}</TableCell>
                    <TableCell align="right">{row.count}</TableCell>
                    <TableCell align="right">${row.priceS}</TableCell>
                    <TableCell align="right">${row.valueS}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const PortfolioViewEx = compose(
  connect(
    mapStateToProps,
    actionCreators
  ),
  withStyles(styles)
)(PortfolioView);

export default PortfolioViewEx;
