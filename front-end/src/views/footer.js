import React from "react";
import { Grid, withStyles } from "@material-ui/core";
import Typography from "../components/typography";

const styles = theme => ({
  h5: {
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilySecondary,
    color: theme.palette.white.main,
    [theme.breakpoints.up("xs")]: {
      fontSize: 12,
    },
    [theme.breakpoints.up("sm")]: {
      fontSize: 16,
    }
  },
  footerGrid: {
    backgroundColor: theme.palette.secondary.main,
    position: "fixed",
    bottom: 0,
    padding: 10,
    height: '50px',
    borderTop: "solid",
    borderWidth: "2px",
    borderColor: theme.palette.secondary.main
  }
});
const PageFooter = props => {
  const { classes } = props;
  return (
    <Grid container direction="column" justify="center" alignItems="center" className={classes.footerGrid}>
      <Grid item xs={12}>
        <Typography variant="button" className={classes.h5}>
          Made by Oliver Robb. You should hire him.
        </Typography>

      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(PageFooter);


