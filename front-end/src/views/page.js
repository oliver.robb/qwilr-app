import React, { Component } from "react";
import { connect } from "react-redux";
import InfoZone from "../views/infoZone";
import TopButton from '../components/topButton'
import * as actionCreators from "../actions/index.js";
import Header from './header'


class CustomPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: props.items,
      desc: {},
    };
  }

  render() {
    const pageTitle = this.props.pageTitle;
    return (
      <React.Fragment>
        <Header title={pageTitle}/>
        <InfoZone  />
        <TopButton />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  actionCreators
)(CustomPage);
