import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "../components/typography"

const styles = theme => ({
  title: {
    color: theme.palette.white.main  }
});

const Loader = props => {
  const { classes } = props;
  return (
    <React.Fragment>
     <Typography variant="h2" className={classes.title}>Loading</Typography>
    </React.Fragment>
  );
};

export default withStyles(styles)(Loader);
