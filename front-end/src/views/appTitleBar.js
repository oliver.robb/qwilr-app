import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import qwilr from "../images/qwilr.png";
import TitleBar from "../components/titleBar";
import Toolbar, { styles as toolbarStyles } from "../components/toolbar";
import { Link as RouterLink } from "react-router-dom";
import LogoutButton from "../components/logoutButton";
import { withFirebase } from "../firebase/";
import { compose } from "recompose";
import { connect } from "react-redux";
import * as actionCreators from "../actions";
import Button from "../components/button";

const styles = theme => ({
  title: {
    fontSize: 24,
    color: theme.palette.common.white
  },
  button: {
    width: 130,
    height: 60,
    fontSize: 15,
    color: theme.palette.white.main,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  },
  image: {
    height: 44
  },
  placeholder: toolbarStyles(theme).root,
  toolbar: {
    justifyContent: "space-between"
  },
  left: {
    flex: 1,
    display: "flex",
    justifyContent: "flex-start"
  },
  leftLinkActive: {
    color: theme.palette.common.white
  },
  right: {
    flex: 1,
    display: "flex",
    justifyContent: "flex-end"
  },
  rightLink: {
    fontSize: 16,
    color: theme.palette.common.white,
    margin: theme.spacing(1)
  },
  linkSecondary: {
    color: theme.palette.secondary.main,
    margin: theme.spacing(1)
  }
});

const userAuthed = classes => {
  return (
    <React.Fragment>
      <div className={classes.left} />

      <RouterLink className={classes.title} to={"/"}>
        <img src={qwilr} className={classes.image} alt="logo" />
      </RouterLink>
      <div className={classes.right}>

        <Button
          className={classes.button}
          component={linkProps => <RouterLink {...linkProps} to={"/portfolio"} />}
        >
          Portfolio
        </Button>
        <Button
          className={classes.button}
          component={linkProps => <RouterLink {...linkProps} to={"/trade"} />}
        >
          Trade
        </Button>
        <LogoutButton />
      </div>

    </React.Fragment>
  );
};

const userNotAuthed = classes => {
  return (
    <React.Fragment>
      <div className={classes.left} />

      <RouterLink className={classes.title} to={"/"}>
        <img src={qwilr} className={classes.image} alt="logo" />
      </RouterLink>
      <div className={classes.right}>
        <Button
          className={classes.button}
          component={linkProps => <RouterLink {...linkProps} to={"/login"} />}
        >
          Log in
        </Button>
        <Button
          className={classes.button}
          component={linkProps => <RouterLink {...linkProps} to={"/register"} />}
        >
          Register
        </Button>
      </div>
    </React.Fragment>
  );
};

class AppTitleBarBase extends Component {
  componentDidMount() {
    this.listener = this.props.firebase.auth.onAuthStateChanged(authUser => {
      if (authUser) {
        this.props.getUser(authUser.uid)
      }
      else {
        this.props.logout();
      }
    });
  }

  componentWillUnmount() {
    this.listener();
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <TitleBar position="fixed">
          <Toolbar className={classes.toolbar}>
            {this.props.user ? userAuthed(classes) : userNotAuthed(classes)}
          </Toolbar>
        </TitleBar>
        <div className={classes.placeholder} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const AppTitleBar = compose(
  withFirebase,
  connect(
    mapStateToProps,
    actionCreators
  )
)(AppTitleBarBase);

AppTitleBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppTitleBar);
