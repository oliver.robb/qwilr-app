import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { withFirebase } from "../firebase/";
import * as ROUTES from "../constants/routes";
import { compose } from "recompose";
import { connect } from "react-redux";
import * as actionCreators from "../actions";

const INITIAL_STATE = {
    name: "",
    email: "",
    passwordOne: "",
    passwordTwo: "",
    error: null
  };
  
  class RegisterFormBase extends Component {
    constructor(props) {
      super(props);
      this.state = { ...INITIAL_STATE };
    }
  
    onSubmit = event => {
      const { email, passwordOne } = this.state;
  
      this.props.firebase
        .doCreateUserWithEmailAndPassword(email, passwordOne)
        .then(authUser => {
          this.props.createUser(authUser.user.uid);
          this.props.history.push(ROUTES.HOME);
        })
        .catch(error => {
          this.setState({ error });
        });
  
      event.preventDefault();
    };
  
    onChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
  
    render() {
      const { name, email, passwordOne, passwordTwo, error } = this.state;
      const isInvalid =
        passwordOne !== passwordTwo ||
        passwordOne === "" ||
        email === "" ||
        name === "";
      return (
        <form onSubmit={this.onSubmit}>
          <input
            name="name"
            value={name}
            onChange={this.onChange}
            type="text"
            placeholder="Full Name"
          />
          <input
            name="email"
            value={email}
            onChange={this.onChange}
            type="text"
            placeholder="Email Address"
          />
          <input
            name="passwordOne"
            value={passwordOne}
            onChange={this.onChange}
            type="password"
            placeholder="Password"
          />
          <input
            name="passwordTwo"
            value={passwordTwo}
            onChange={this.onChange}
            type="password"
            placeholder="Confirm Password"
          />
          <button disabled={isInvalid} type="submit">
            Sign Up
          </button>
  
          {error && <p>{error.message}</p>}
        </form>
      );
    }
  }
  
  const RegisterLink = () => (
    <p>
      Don't have an account? <Link to={ROUTES.REGISTER}>Sign Up</Link>
    </p>
  );

  const mapStateToProps = state => {
    return state;
  };
  
  const RegisterForm = compose(
    withRouter,
    connect(
      mapStateToProps,
      actionCreators
    ),
    withFirebase
  )(RegisterFormBase);

  export default RegisterForm;

  export { RegisterLink };

