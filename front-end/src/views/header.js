import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "../components/typography";
import { Grid } from "@material-ui/core";

const styles = theme => ({
  h1: {
    marginBottom: theme.spacing(10),
    marginTop: theme.spacing(8),
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(10)
    }
  },
});

function Header(props) {
  const { title, classes } = props;

  return (
    <Grid container alignItems="center" justify="center">
      <Grid item>
        <Typography
          color="primary"
          align="center"
          variant="h1"
          marked="center"
          className={classes.h1}
        >
          {title}
        </Typography>
      </Grid>
    </Grid>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
