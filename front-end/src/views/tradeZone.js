import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "../components/typography";
import Button from "../components/button";
import {
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Table,
  InputLabel,
  Select,
  MenuItem,
  Paper,
  TableCell,
  TableHead,
  TableRow,
  TableBody,
  TextField
} from "@material-ui/core";
import { connect } from "react-redux";
import * as actionCreators from "../actions";
import { compose } from "recompose";
import Axios from "axios";
import { withRouter } from "react-router-dom";

const styles = theme => ({
  h1: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(2)
    }
  },
  button: {
    width: 210,
    height: 75,
    fontSize: 16,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.primary.main,
    borderWidth: 2,
    borderColor: theme.palette.secondary.dark,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  },
  sellFormControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  }
});

class TradeZone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBuy: "buy",
      sellStocks: [],
      rows: [],
      searchError: null,
      selectedSellStock: null,
      sellStockNumber: null,
      selectedBuyStock: null,
      buyStockNumber: null,
      maxBuyCount: null,
      buyStockPrice: null,
      error: null
    };
  }

  handleRadioChange = event => {
    this.setState({ isBuy: event.target.value, error: null });
  };

  handleStockSelection = event => {
    if (event) {
      this.setState({ selectedSellStock: event.target.value });
      console.log(this.state.selectedSellStock);
    }
  };

  handleSSNChange = event => {
    let value = event.target.value;

    if (event) {
      this.setState({ sellStockNumber: value });
    }
  };

  handleSBNChange = event => {
    let value = event.target.value;

    if (event) {
      this.setState({ buyStockNumber: value });
    }
  };

  handleBuyStockInput = event => {
    if (event) {
      let value = event.target.value;
      this.setState({
        selectedBuyStock: value.toUpperCase(),
        buyStockPrice: null,
        maxBuyCount: null
      });
    }
  };

  searchSelectedStock = async () => {
    let apikey = "2MZ0F3RWHCS07MFK";
    let url = `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${
      this.state.selectedBuyStock
    }&apikey=${apikey}`;
    let price = -1;
    await Axios.get(url)
      .then(stock => {
        price = stock.data["Global Quote"]["05. price"];
        this.setState({
          buyStockPrice: price,
          maxBuyCount: parseInt(this.props.cashBalance / price)
        });
      })
      .catch(e => {
        this.setState({
          searchError:
            "Couldn't find specified stock. If you're looking for something on the ASX, try something like 'BHP.AX'"
        });
      });

    return price;
  };

  isBuyValid = () => {
    return !(this.state.selectedBuyStock && this.state.buyStockNumber);
  };

  calculateMaxBuy = () => {
    return !(this.state.maxBuyCount && this.state.BuyStockNumber);
  };

  sellSelectedStock = async () => {
    let maxNum = this.props.portfolio[this.state.selectedSellStock].count;
    let minNum = 1;
    if (this.state.sellStockNumber < minNum) {
      this.setState({ error: "Can't sell less than 1 share." });
    } else if (this.state.sellStockNumber > maxNum) {
      this.setState({ error: "Can't sell more shares than you own." });
    } else {
      let shareCode = this.state.selectedSellStock;
      let portfolioCopy = this.props.portfolio;
      let oldPrice = portfolioCopy[shareCode].price;
      let currCount = portfolioCopy[shareCode].count;
      let currentPrice = await this.getAxios(shareCode, oldPrice);
      let newCB =
        this.props.cashBalance + this.state.sellStockNumber * currentPrice;
      if (this.state.sellStockNumber - currCount === 0) {
        delete portfolioCopy[shareCode];
      } else {
        portfolioCopy[shareCode].count = currCount - this.state.sellStockNumber;
        portfolioCopy[shareCode].price = currentPrice;
        portfolioCopy[shareCode].value =
          currentPrice * (currCount - this.state.sellStockNumber);
      }
      this.props.updateUser(this.props.user, newCB, portfolioCopy);
      this.setState({ selectedSellStock: null, sellStockNumber: null });
    }
  };

  buySelectedStock = async () => {
    let minNum = 1;
    if (this.state.buyStockNumber < minNum) {
      this.setState({ error: "Can't buy less than 1 share." });
    } else if (this.state.buyStockNumber > this.state.maxBuyCount) {
      this.setState({
        error: `Can't buy more than ${
          this.state.maxBuyCount
        } shares. Try adding to your cash balance if you want this amount.`
      });
    } else {
      let portfolioCopy = this.props.portfolio;
      let shareCode = this.state.selectedBuyStock;
      let newCB =
        this.props.cashBalance -
        this.state.buyStockNumber * this.state.buyStockPrice;
      let count = parseInt(this.state.buyStockNumber);
      let price = parseFloat(this.state.buyStockPrice);
      let value = count * price;
      if (!this.state.sellStocks.includes(shareCode)) {
        portfolioCopy[shareCode] = {
          count: count,
          cost: price,
          prev: price,
          value: value
        };
      }
      else {
        let existing = portfolioCopy[shareCode]
        let oldCount = existing.count
        let oldCost = existing.cost
        existing.count = existing.count + count
        existing.prev = price
        existing.cost = (oldCost * oldCount / (count + oldCount)) + (price * count / (count + oldCount))
        existing.value = existing.count * price
      }

      this.props.updateUser(this.props.user, newCB, portfolioCopy);
      this.setState({
        selectedBuyStock: null,
        buyStockNumber: null,
        maxBuyCount: null,
        buyStockPrice: null
      });
    }
  };

  sellZone = () => {
    const { classes } = this.props;
    return (
      <React.Fragment>
        {this.state.error && <Typography>{this.state.error}</Typography>}
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Stock</TableCell>
                <TableCell align="right">Purchase Price ($/share)</TableCell>
                <TableCell align="right">Count</TableCell>
                <TableCell align="right">Price ($/share)</TableCell>
                <TableCell align="right">Value ($)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.rows.map(row => (
                <TableRow key={row.code}>
                  <TableCell align="left">{row.code}</TableCell>
                  <TableCell align="right">${row.costS}</TableCell>
                  <TableCell align="right">{row.count}</TableCell>
                  <TableCell align="right">${row.priceS}</TableCell>
                  <TableCell align="right">${row.valueS}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Typography>Select stock and quantity to sell.</Typography>
            <Grid item xs={12}>
              <Grid container row alignItems="center" justify="center">
                <Grid item>
                  <FormControl className={classes.sellFormControl}>
                    <InputLabel htmlFor="age-simple">Stock</InputLabel>
                    <Select
                      value={this.state.selectedSellStock}
                      onChange={this.handleStockSelection}
                      inputProps={{
                        name: "Stock",
                        id: "stock"
                      }}
                    >
                      {this.state.sellStocks.map(stock => {
                        return <MenuItem value={stock}>{stock}</MenuItem>;
                      })}
                    </Select>
                    <TextField
                      id="standard-number"
                      label="Quantity"
                      value={this.state.sellStockNumber}
                      onChange={this.handleSSNChange}
                      disabled={!this.state.selectedSellStock}
                      type="number"
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true
                      }}
                      margin="normal"
                    />
                    {this.state.error && (
                      <Typography>{this.state.error}</Typography>
                    )}
                    <Button
                      className={classes.button}
                      disabled={
                        !(
                          this.state.selectedSellStock &&
                          this.state.sellStockNumber
                        )
                      }
                      onClick={this.sellSelectedStock}
                    >
                      Submit Sale
                    </Button>
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  };

  buyZone = () => {
    const { classes } = this.props;
    return (
      <React.Fragment>
        {this.state.error && <Typography>{this.state.error}</Typography>}
        <Grid container spacing={1} alignItems="center" justify="center">
          <Grid item xs={12}>
            <Typography>
              Search a stock (e.g. MSFT, BHP.AX), adjust the number of shares
              you want to purchase, and submit!{" "}
            </Typography>
            <Grid item xs={12}>
              <Grid container row alignItems="center" justify="center">
                <Grid item>
                  <FormControl className={classes.sellFormControl}>
                    <TextField
                      id="standard-number"
                      label="Stock Code"
                      value={this.state.selectedBuyStock}
                      onChange={this.handleBuyStockInput}
                      type="search"
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true
                      }}
                      margin="normal"
                    />
                    {this.state.searchError && (
                      <Typography>{this.state.searchError}</Typography>
                    )}

                    <Button
                      className={classes.button}
                      disabled={!this.state.selectedBuyStock}
                      onClick={this.searchSelectedStock}
                    >
                      Search
                    </Button>

                    {this.state.buyStockPrice && (
                      <Typography>
                        The current price for {this.state.selectedBuyStock} is $
                        {this.state.buyStockPrice}.
                      </Typography>
                    )}
                    {this.state.buyStockPrice && (
                      <Typography>
                        You can afford {this.state.maxBuyCount} share(s) given
                        your current Cash Balance.
                      </Typography>
                    )}

                    <TextField
                      id="standard-number"
                      label="Quantity"
                      value={this.state.buyStockNumber}
                      onChange={this.handleSBNChange}
                      disabled={!this.state.maxBuyCount}
                      type="number"
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true
                      }}
                      margin="normal"
                    />
                    {this.state.error && (
                      <Typography>{this.state.error}</Typography>
                    )}
                    <Button
                      className={classes.button}
                      disabled={this.isBuyValid()}
                      onClick={this.buySelectedStock}
                    >
                      Submit Purchase
                    </Button>
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  };

  componentWillReceiveProps = async ({ portfolio }) => {
    let data = [];
    let stockNames = [];
    if (portfolio) {
      for (var key in portfolio) {
        stockNames.push(key);
        if (portfolio.hasOwnProperty(key)) {
          let item = portfolio[key];
          data.push(
            await this.createData(key, item.cost, item.count, item.prev)
          );
        }
      }
    }
    if (stockNames.length > 0) {
      let sortedStockNames = stockNames.sort();
      this.setState({ sellStocks: sortedStockNames });
    }
    if (data.length > 0) {
      this.setState({ rows: data });
    }
  };

  getAxios = async (code, prev_high) => {
    let apikey = "2MZ0F3RWHCS07MFK";
    let url = `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${code}&apikey=${apikey}`;
    let price = -1;
    await Axios.get(url)
      .then(stock => {
        price = stock.data["Global Quote"]["05. price"];
      })
      .catch(e => {
        console.log(e);
        price = prev_high;
      });

    return price;
  };

  componentWillUnmount = () => {
    let data = {};
    this.state.rows.map(stock => {
      console.log(stock);
      let stockInfo = {
        cost: stock.cost,
        count: stock.count,
        prev: stock.price,
        value: stock.price * stock.count
      };
      console.log(stockInfo);
      data[stock.code] = stockInfo;
    });

    this.props.updateUser(this.props.user, this.props.cashBalance, data);
  };

  createData = async (code, cost, count, prev) => {
    let price = await this.getAxios(code, prev);
    let value = count * price;
    let valueS = (value * 1).toFixed(2);
    let priceS = (1 * price).toFixed(2);
    let costS = (cost * 1).toFixed(2);
    return { code, cost, count, price, value, costS, priceS, valueS };
  };

  render() {
    const { classes } = this.props;

    return (
      <Grid container column alignItems="center" justify="center">
        <Grid item xs={12}>
          <Typography
            color="secondary"
            align="center"
            variant="h2"
            marked="center"
            className={classes.h1}
          >
            Buy / Sell
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container row alignItems="center" justify="center">
            <Grid item>
              <FormControl component="fieldset" className={classes.formControl}>
                <RadioGroup
                  row
                  aria-label="Buy or Sell"
                  name="buySell"
                  className={classes.group}
                  value={this.state.isBuy}
                  onChange={this.handleRadioChange}
                >
                  <FormControlLabel
                    value="buy"
                    control={<Radio />}
                    label="Buy"
                  />
                  <FormControlLabel
                    value="sell"
                    control={<Radio />}
                    label="Sell"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
          </Grid>
        </Grid>
        {this.state.isBuy === "buy" && this.buyZone()}
        {this.state.isBuy === "sell" && this.sellZone()}
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const TradeZoneEx = compose(
  connect(
    mapStateToProps,
    actionCreators
  ),
  withRouter,
  withStyles(styles)
)(TradeZone);

export default TradeZoneEx;
