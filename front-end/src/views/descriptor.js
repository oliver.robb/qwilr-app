import React from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Typography from '../components/typography';

const styles = theme => ({
  span: {
    color: theme.palette.white.main,
  },
  grid: {
    borderWidth: 4,
    border: "solid",
    borderColor: theme.palette.primary.main,
    backgroundColor: theme.palette.secondary.dark,
    padding: theme.spacing(3),
    marginBottom: theme.spacing(3)
  }
});

const CustomDescriptor = props => {
  const { body, classes } = props; // 1

  return (
    <Grid container direction="row" justify="center" alignItems="center">
      <Grid item className={classes.grid} xs={8}>
        <Typography variant="body1" className={classes.span}>
          <b>{body}</b>
        </Typography>
      </Grid>
    </Grid>
    // <p className="custom-descriptor"><span className="custom-descriptor-text"><b>{body}</b></span></p>
  );
};

export default withStyles(styles)(CustomDescriptor);
