import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionCreators from "../actions/index.js";
import { withRouter } from "react-router-dom";
import { Link as RouterLink } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Button from "../components/button";
import { Grid } from "@material-ui/core";


const styles = theme => ({
  button: {
    width: 210,
    height: 75,
    fontSize: 16,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.primary.main,
    borderWidth: 2,
    borderColor: theme.palette.secondary.dark,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  },
  container: {
    marginTop: theme.spacing(5),
    marginRight: 0
  }
});

class ButtonGroup extends Component {
   incCB = event =>  {
    let newBal = this.props.cashBalance + 1000
    let uid = this.props.user
    let portfolio = this.props.portfolio
    this.props.updateUser(uid, newBal, portfolio)
    event.preventDefault();
  }
   decCB = event =>  {
    let newBal = Math.max(this.props.cashBalance - 1000, 0)
    let uid = this.props.user
    let portfolio = this.props.portfolio
    this.props.updateUser(uid, newBal, portfolio)
    event.preventDefault();
  }

  render() {
    const { classes } = this.props;
    return (
      <div style={{ padding: 8 }}>
        <Grid container className={classes.container}>
          <Grid item xs={12}>
            <Grid
              container
              spacing={1}
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <Button
                  size="large"
                  variant="outlined"
                  className={classes.button}
                  onClick={this.incCB}
                >
                  Deposit $1000
                </Button>
              </Grid>
              <Grid item>
                <Button
                  size="large"
                  variant="outlined"
                  className={classes.button}
                  onClick={this.decCB}
                >
                  Withdraw $1000
                </Button>
              </Grid>
              <Grid item>
                <Button
                  size="large"
                  variant="outlined"
                  className={classes.button}
                  component={linkProps => (
                    <RouterLink {...linkProps} to={`/trade`} />
                  )}
                >
                  Trade
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default withRouter(
  connect(
    mapStateToProps,
    actionCreators
  )(withStyles(styles)(ButtonGroup))
);
