import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import CssBaseline from "@material-ui/core/CssBaseline";

import * as ROUTES from "./constants/routes";

import App from "./routes/app";
import RegisterPage from "./routes/register/";
import LoginPage from "./routes/login/";
import TradePage from "./routes/trade/";
import PortfolioPage from "./routes/portfolio/";


import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers/index";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";
import "typeface-roboto";
import Firebase, { FirebaseContext } from "./firebase/index";

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk))
);

ReactDOM.render(
  <FirebaseContext.Provider value={new Firebase()}>
    <MuiThemeProvider theme={theme}>
      <CssBaseline />

      <Provider store={store}>
        <Router>
          <div>
            <Route exact path={ROUTES.HOME} component={App} />
            <Route exact path={ROUTES.REGISTER} component={RegisterPage} />
            <Route exact path={ROUTES.LOGIN} component={LoginPage} />
            <Route exact path={ROUTES.TRADE} component={TradePage} />
            <Route exact path={ROUTES.PORTFOLIO} component={PortfolioPage} />
          </div>
        </Router>
      </Provider>
    </MuiThemeProvider>
  </FirebaseContext.Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
