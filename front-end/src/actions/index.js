import Axios from "axios";

export function createUser(uid) {
  let url = `https://us-central1-qwilr-app-37b2d.cloudfunctions.net/createUser?uid=${uid}`;
  return dispatch => {
    return Axios.post(url).then(response => {
      dispatch(setStateFromGet(0, "SET_CASHBALANCE"));
    });
  };
}

export function getUser(uid) {
  let url = `https://us-central1-qwilr-app-37b2d.cloudfunctions.net/getUser?uid=${uid}`;

  return dispatch => {
    return Axios.get(url).then(user => {
      dispatch(setStateFromGet(uid, "SET_USER"));
      dispatch(setStateFromGet(user.data.cashBalance, "SET_CASHBALANCE"));
      dispatch(setStateFromGet(user.data.portfolio, "SET_PORTFOLIO"));
    });
  };
}

export function updateUser(uid, cashBalance, portfolio) {
  let url = `https://us-central1-qwilr-app-37b2d.cloudfunctions.net/updateUser?uid=${uid}`;
  let data = {
    cashBalance: cashBalance,
  }
  if (portfolio) {
    data.portfolio = portfolio
  }
  return dispatch => {
    return Axios.post(url, data).then(response => {
      dispatch(setStateFromGet(cashBalance, "SET_CASHBALANCE"));
      dispatch(setStateFromGet(portfolio, "SET_PORTFOLIO"));
    });
  };
}

export function setUserID(user) {
  let uid = user ? user.uid : null;
  return dispatch => {
    dispatch(setStateFromGet(uid, "SET_USER"));
  };
}

export function logout() {
  return dispatch => {
    dispatch(setStateFromGet(null, "LOG_OUT"));
  };
}

export function setStateFromGet(res, type) {
  return {
    type: type,
    res: res
  };
}
