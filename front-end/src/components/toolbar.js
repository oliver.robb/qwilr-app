import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';

export const styles = theme => ({
  root: {
    padding: 8,
    margin: theme.spacing(1),
    height: 64,
  },
});

export default withStyles(styles)(Toolbar);