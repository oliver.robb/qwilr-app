import React, { Component } from "react";
import { animateScroll as scroll } from "react-scroll";
import Fab from "@material-ui/core/Fab";
import UpIcon from "@material-ui/icons/KeyboardArrowUp";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

const styles = theme => ({
  fab: {
    margin: theme.spacing(1), // You might not need this now
    position: "fixed",
    bottom: theme.spacing(7),
    right: theme.spacing(1),
    color: theme.palette.secondary.main,
    backgroundColor: theme.palette.primary.main,
    zIndex: 1,
    '&:hover': {
      backgroundColor: theme.palette.secondary.dark,
      color:  theme.palette.primary.main,
    },
  }
});

class TopButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTop: true
    };
    this.scrollToTop = this.scrollToTop.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount = () => {
    window.addEventListener("scroll", this.handleScroll);
  };

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.handleScroll);
  };

  scrollToTop = () => {
    scroll.scrollToTop();
  };

  handleScroll = (event) => {
    this.setState({
      isTop: window.pageYOffset < 250
    });
  };

  render() {
    if (!this.state.isTop) {
      return (      
        <Fab onClick={this.scrollToTop} className={this.props.classes.fab}>
          <UpIcon />
        </Fab>
      );
    }
    return null;   
  }
}

TopButton.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TopButton);
