import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  background: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.secondary.dark,
  }
});

function Page(props) {
  const { children, classes } = props;

  return (
    <div style={{ padding: 20 }}>
      <Grid
        container
        spacing={1}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item md={10} xs={10} sm={10} lg={8} xl={7}>
          <Grid
            className={classes.background}
            container
            spacing={2}
            direction="row"
            justify="center"
            alignItems="center"
          >
            {children}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

Page.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Page);
