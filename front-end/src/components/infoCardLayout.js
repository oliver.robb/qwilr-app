import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import LayoutBody from './layoutBody';
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
  root: {
    color: theme.palette.common.white,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
  },
  layoutBody: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  backdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.6,
    zIndex: -1,
  },
  background: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    zIndex: -2,
  },
  infoCardOuterGrid: {

  },
  
});

function DummyLayout(props) {
  const { backgroundClassName, children, classes, backgroundImage } = props;

  return (
    <Grid item xs={11} sm={10} className={classes.infoCardOuterGrid}>
    <section className={classes.root}>
    <Grid
        container
        justify="center"
        alignItems="center"
      >
      <LayoutBody className={classes.layoutBody} fullWidth={true} >
        {children}
        <div className={classes.backdrop} />
        <div className={classNames(classes.background, backgroundClassName)} style={{backgroundImage}} />
      </LayoutBody>
      </Grid>
    </section>
    </Grid>
  );
}

DummyLayout.propTypes = {
  backgroundClassName: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DummyLayout);