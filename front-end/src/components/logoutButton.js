import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withFirebase } from "../firebase";
import Button from "./button";
import * as ROUTES from "../constants/routes";
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import * as actionCreators from "../actions";
import { compose } from "recompose";


const styles = theme => ({
  outlinedButton: {
    width: 140,
    height: 60,
    fontSize: 15,
    color: theme.palette.white.main,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  }
});


class LogoutButton extends Component {

  onLogout = () => {
    this.props.firebase.doLogout()
    this.props.logout()
    this.props.history.push(ROUTES.HOME);
  };

  render() {
    const { classes } = this.props;
    return (
      <Button
        className={classes.outlinedButton}
        onClick={this.onLogout}
      >
        Log out
      </Button>
    );
  }
}

const mapStateToProps = state => {
  return state;
};


const LogoutButtonEx = compose(
  withRouter,
  withFirebase,
  connect(
    mapStateToProps,
    actionCreators
  )
)(LogoutButton);


export default withStyles(styles)(LogoutButtonEx);
