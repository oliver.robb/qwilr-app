import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "../components/typography";
import ICLayout from "./infoCardLayout";

const styles = theme => ({
  background: {
    backgroundColor: "#7fc7d9", // Average color of the background image.
    backgroundPosition: "center"
  },
  button: {
    minWidth: 200
  },
  h5: {
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4),
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(10)
    }
  },
  blurb: {
    fontSize: 20,
    color: theme.palette.secondary.dark,
    marginBottom: theme.spacing(10),
    marginTop: theme.spacing(10),
    [theme.breakpoints.down("xs")]: {
      fontSize: 16,
      fontWeight: "medium"
    }
  },
  more: {
    marginTop: theme.spacing(2),
  },
  text: {
    textAlign: "justify",
    color: theme.palette.white.main,
    marginBottom: theme.spacing(1),
    [theme.breakpoints.down("xs")]: {
      fontSize: 14,
      fontWeight: "medium",
      textAlign: "left"
    }
  },

  
});

const InfoCardDummy = props => {
  const { classes, title, blurb, imageUrl, children } = props;
  return (
    <ICLayout
      backgroundClassName={classes.background}
      backgroundImage={`url(${imageUrl})`}
    >
      <img style={{ display: "none" }} src={imageUrl} alt="" />
      {title &&
      <Typography color="inherit" align="center" variant="h2" marked="center" className={classes.h2}>
        {title}
      </Typography>}
      { blurb && <Typography
        color="inherit"
        align="center"
        variant="body2"
        className={classes.blurb}
        >
        {blurb}
      </Typography>
      }
      {children}
    </ICLayout>
  );
};

InfoCardDummy.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(InfoCardDummy);
