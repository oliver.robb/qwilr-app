import React from "react";
import qwilr from "../../images/qwilr.png";
import "../../css/Home.css";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "typeface-roboto";
import { Grid, withStyles } from "@material-ui/core";
import Typography from "../../components/typography";
import AppTitleBar from "../../views/appTitleBar";
import PageFooter from "../../views/footer";

const styles = theme => ({
  h1: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(5),
    [theme.breakpoints.up("xs")]: {
      fontSize: 24,
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2)
    },
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(10),
      fontSize: 64
    },
    [theme.breakpoints.up("lg")]: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(2)
    }
  },
  subtitle: {
    marginBottom: theme.spacing(10),
    color: theme.palette.secondary.main,
    [theme.breakpoints.up("xs")]: {
      fontSize: 16,
      marginBottom: theme.spacing(2)
    },
    [theme.breakpoints.up("sm")]: {
      fontSize: 24
    }
  },
  imageDiv: {
    [theme.breakpoints.up("xs")]: {
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(1)
    },
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(12),
      marginBottom: theme.spacing(2)
    },
    [theme.breakpoints.up("lg")]: {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(2)
    }
  }
});

const App = props => {
  const { classes } = props;
  return (
    <div className="App">
      <header className="App-header">
        <AppTitleBar />
        <Grid container alignItems="center" justify="center" direction="column">
          <Grid item xs={6} sm={5} md={4} lg={3} className={classes.imageDiv}>
            <img src={qwilr} className="App-logo" alt="logo" />
          </Grid>
          <Grid item xs={11}>
            <Typography
              color="primary"
              align="center"
              marked="center"
              variant="h1"
              className={classes.h1}
            >
              Visualisr
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <Typography
              color="secondary"
              align="center"
              variant="subtitle1"
              className={classes.subtitle}
            >
              Effortlessly curate and administer your stock portfolio with
              Visualisr.
            </Typography>
          </Grid>
        </Grid>
      </header>
      <PageFooter />
      <div />
    </div>
  );
};

const mapStateToProps = state => {
  return state;
};

export default withRouter(connect(mapStateToProps)(withStyles(styles)(App)));
