import React, { Component } from "react";
import AppTitleBar from "../../views/appTitleBar";
import Header from "../../views/header";
import TopButton from "../../components/topButton";
import PageFooter from "../../views/footer";
import PortfolioButtonGroup from "../../views/portfolioButtonGroup";
import InfoCard from "../../components/infoCard";
import Page from "../../components/page";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import * as actionCreators from "../../actions";
import { compose } from "recompose";
import PortfolioView from "../../views/portfolioView";

const styles = theme => ({
  button: {
    width: 210,
    height: 75,
    fontSize: 16,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.primary.main,
    borderWidth: 2,
    borderColor: theme.palette.secondary.dark,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  },
  container: {
    marginLeft: 0,
    marginRight: 0
  },
  caption: {
    fontSize: 20
  },
  cashBal: {
    fontSize: 60
  }
});

class PortfolioPage extends Component {
  calcValue() {
    let portfolio = this.props.portfolio;
    let sum = 0
    for (var key in portfolio) {
      if (portfolio.hasOwnProperty(key)) {
        let item = portfolio[key];
        sum = sum + (item.value)
      }
    }
    return sum.toFixed(2)
  }

  calcTotalValue() {
    let totalVal = parseFloat(this.calcValue()) + this.props.cashBalance
    return (totalVal).toFixed(2)
  }

  getCB = () => {
    return (this.props.cashBalance * 1).toFixed(2)
  }


  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <AppTitleBar />
        <Header title="Portfolio" />
        <Page>
          <InfoCard>
            <Grid
              container
              spacing={2}
              direction="row"
              alignItems="center"
              justify="center"
            >
              <Grid item xs={7} md={9}>
                <Grid container justify="flex-end">
                  <Grid item className={classes.caption}>
                    Cash Balance:{" "}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={5} md={3} className={classes.cashBal}>
                ${this.getCB()}
              </Grid>
              <Grid item xs={7} md={9}>
                <Grid container justify="flex-end">
                  <Grid item className={classes.caption}>
                    Portfolio Value:{" "}
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={5} md={3} className={classes.cashBal}>
                ${this.calcValue()}
              </Grid>
              <Grid item xs={7} md={9}>
                <Grid container justify="flex-end">
                  <Grid item className={classes.caption}>
                    Total Value:{" "}
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={5} md={3} className={classes.cashBal}>
                ${this.calcTotalValue()}
              </Grid>
            </Grid>
            <PortfolioButtonGroup />
          </InfoCard>
        </Page>
        <Page>
          <InfoCard>
            <PortfolioView />
          </InfoCard>
        </Page>
        <TopButton />
        <PageFooter />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const PortfolioPageEx = compose(
  connect(
    mapStateToProps,
    actionCreators
  ),
  withStyles(styles)
)(PortfolioPage);

export default withStyles(styles)(PortfolioPageEx);
