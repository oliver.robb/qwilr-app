import React from "react";
import AppTitleBar from "../../views/appTitleBar"
import Header from "../../views/header"
import TopButton from "../../components/topButton"
import RegisterForm from "../../views/registerForm"
import PageFooter from "../../views/footer";


const RegisterPage = () => (
  <div>
    <React.Fragment>
      <AppTitleBar />
      <Header title="Register" />
      <RegisterForm />
      <TopButton />
      <PageFooter />
    </React.Fragment>
  </div>
);

export default RegisterPage;

