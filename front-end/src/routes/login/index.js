import React from "react";
import AppTitleBar from "../../views/appTitleBar";
import Header from "../../views/header";
import TopButton from "../../components/topButton";
import LoginForm from "../../views/loginForm";
import PageFooter from "../../views/footer";


const LoginPage = () => (
  <div>
    <React.Fragment>
      <AppTitleBar />
      <Header title="log in" />
      <LoginForm />
      <TopButton />
      <PageFooter />
    </React.Fragment>
  </div>
);

export default LoginPage;
