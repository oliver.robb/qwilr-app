import React, { Component } from "react";
import AppTitleBar from "../../views/appTitleBar";
import Header from "../../views/header";
import TopButton from "../../components/topButton";
import PageFooter from "../../views/footer";
import TradeButtonGroup from "../../views/tradeButtonGroup";
import InfoCard from "../../components/infoCard";
import Page from "../../components/page";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import * as actionCreators from "../../actions";
import { compose } from "recompose";
import TradeZone from "../../views/tradeZone"

const styles = theme => ({
  button: {
    width: 210,
    height: 75,
    fontSize: 16,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.primary.main,
    borderWidth: 2,
    borderColor: theme.palette.secondary.dark,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark
    }
  },
  container: {
    marginLeft: 0,
    marginRight: 0
  },
  caption: {
    fontSize: 20
  },
  cashBal: {
    fontSize: 60
  }
});

class TradePage extends Component {
  getCB = () => {
    return (this.props.cashBalance * 1).toFixed(2)
  }

  render() {
    const { classes } = this.props;
    return (
    <React.Fragment>
      <AppTitleBar />
      <Header title="Trade" />
      <Page>
            <InfoCard>
              <Grid
                container
                spacing={2}
                direction="row"
                alignItems="center"
                justify="center"
              >
                <Grid item xs={8} md={10}>
                  <Grid container justify="flex-end">
                    <Grid item className={classes.caption}>
                      Cash Balance:{" "}
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={4} md={2} className={classes.cashBal}>
                  ${this.getCB()}
                </Grid>
              </Grid>
              <TradeButtonGroup />
            </InfoCard>
          </Page>
          <Page>
          <InfoCard>
            <TradeZone />
          </InfoCard>
          </Page>
      <TopButton />
      <PageFooter />
    </React.Fragment>
); }}

const mapStateToProps = state => {
  return state;
};

const TradePageEx = compose(
  connect(
    mapStateToProps,
    actionCreators
  ),
  withStyles(styles)
)(TradePage);

export default withStyles(styles)(TradePageEx);