import app from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAEX7RYeO2Cs34q9wDUQEeAEVqJQd9VEL4",
  authDomain: "qwilr-app-37b2d.firebaseapp.com",
  databaseURL: "https://qwilr-app-37b2d.firebaseio.com",
  projectId: "qwilr-app-37b2d",
  storageBucket: "qwilr-app-37b2d.appspot.com",
  messagingSenderId: "581194399498",
  appId: "1:581194399498:web:b523c6b0071bfc61"
};

class Firebase {
  constructor() {
    app.initializeApp(firebaseConfig);
    this.auth = app.auth();
  }

  // *** Auth API ***

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  doLoginWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doLogout = () => this.auth.signOut();
}

export default Firebase;
