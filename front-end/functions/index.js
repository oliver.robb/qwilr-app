/* eslint-disable promise/always-return */
const functions = require("firebase-functions"),
  users = require("./users");

module.exports = {
    createUser: functions.https.onRequest(users.createUser),
    getUser: functions.https.onRequest(users.getUser),
    updateUser: functions.https.onRequest(users.updateUser),
};
