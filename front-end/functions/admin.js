const functions = require('firebase-functions'),
    admin = require('firebase-admin');

admin.initializeApp();
const db = admin.firestore();

module.exports = {
  db,
}