const user = require("./user");

module.exports = {
  getUser: user.getUser,
  createUser: user.createUser,
  updateUser: user.updateUser,
};
