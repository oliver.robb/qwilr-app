/* eslint-disable promise/always-return */
const { db } = require("../admin"),
  functions = require("firebase-functions"),
  cors = require("cors")({
    origin: true
  });

const getUser = functions.https.onRequest((req, res) => {
  let uid = req.query.uid;
  cors(req, res, () => {
    db.collection("data")
      .doc(uid)
      .get()
      .then(doc => {
        return res.status(200).json(doc.data());
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
        return res.status(400);
      });
  });
});

const createUser = functions.https.onRequest((req, res) => {
  let uid = req.query.uid;
  cors(req, res, () => {
    db.collection("data")
      .doc(uid)
      .set({
        cashBalance: 0
      })
      .then(doc => {
        console.log(doc)
        return res.status(200).send(doc);
      })
      .catch(error => {
        console.log("Error creating user document: ", error);
        return res.status(400);
      });
  });
});

const updateUser = functions.https.onRequest((req, res) => {
  let uid = req.query.uid;
  console.log(req.body)
  cors(req, res, () => {
    db.collection("data")
      .doc(uid)
      .update(req.body)
      .then(doc => {
        console.log(doc)
        return res.status(200).send(doc);
      })
      .catch(error => {
        console.log("Error updating user document: ", error);
        return res.status(400);
      });
  });
});

const User = { getUser, createUser, updateUser };

module.exports = User;
